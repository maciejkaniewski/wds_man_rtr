#include "mainwindow.hh"

#include <QDebug>
#include <QEvent>
#include <QSerialPortInfo>

#include "manipulator.hh"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *pParent)
    : QMainWindow(pParent), _pUi(new Ui::MainWindow) {
  _pUi->setupUi(this);
  _pUi->groupBox_Inverse_Kinematics->hide();
  _pUi->pushButton_inverse_kinematics_set->setEnabled(false);
  _pUi->red_color->hide();
  _pUi->green_color->hide();
  _pUi->blue_color->hide();
  this->_pDevice = new QSerialPort(this);
}

MainWindow::~MainWindow() { delete _pUi; }

void MainWindow::changeEvent(QEvent *pEvent) {
  if (pEvent->type() == QEvent::LanguageChange) {
    _pUi->retranslateUi(this);
    return;
  }
  QMainWindow::changeEvent(pEvent);
}

void MainWindow::on_pushButton_Q1inc_pressed() {
  this->_ManualControlFrame.replace(2, 1, "1");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q1inc_released() {
  this->_ManualControlFrame.replace(2, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q1dec_pressed() {
  this->_ManualControlFrame.replace(2, 1, "2");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q1dec_released() {
  this->_ManualControlFrame.replace(2, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q2inc_pressed() {
  this->_ManualControlFrame.replace(2, 1, "3");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q2inc_released() {
  this->_ManualControlFrame.replace(2, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q2dec_pressed() {
  this->_ManualControlFrame.replace(2, 1, "4");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q2dec_released() {
  this->_ManualControlFrame.replace(2, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q3inc_pressed() {
  this->_ManualControlFrame.replace(4, 1, "1");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q3inc_released() {
  this->_ManualControlFrame.replace(4, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q3dec_pressed() {
  this->_ManualControlFrame.replace(4, 1, "2");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_Q3dec_released() {
  this->_ManualControlFrame.replace(4, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_CloseGripper_clicked() {
  this->_ManualControlFrame.replace(6, 1, "1");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on_pushButton_OpenGripper_clicked() {
  this->_ManualControlFrame.replace(6, 1, "0");
  this->sendMessageToDevice(_ManualControlFrame);
}

void MainWindow::on__pAction_Connection_triggered() {
  this->_pDialogConnection = new DialogConnection();
  _pDialogConnection->setWindowModality(Qt::ApplicationModal);
  _pDialogConnection->show();
  connect(this->_pDialogConnection, SIGNAL(sendSerialPortName(QString)), this,
          SLOT(connectSerialPort(QString)));
  connect(this->_pDialogConnection, SIGNAL(sendDisconnect()), this,
          SLOT(disconnectSerialPort()));
  connect(this->_pDialogConnection, SIGNAL(sendToLogs(QString)), this,
          SLOT(addToLogs(QString)));
}

void MainWindow::on__pAction_Info_triggered() {
  this->_pDialogInfo = new DialogInfo();
  _pDialogInfo->setWindowModality(Qt::ApplicationModal);
  _pDialogInfo->show();
}

void MainWindow::connectSerialPort(QString PortName) {
  this->_pDevice->setPortName(PortName);

  if (!_pDevice->isOpen()) {
    if (_pDevice->open(QSerialPort::ReadWrite)) {
      this->_pDevice->setBaudRate(QSerialPort::Baud115200);
      this->_pDevice->setDataBits(QSerialPort::Data8);
      this->_pDevice->setParity(QSerialPort::NoParity);
      this->_pDevice->setStopBits(QSerialPort::OneStop);
      this->_pDevice->setFlowControl(QSerialPort::NoFlowControl);

      addToLogs(tr("Otwarto port szeregowy."));
      addToLogs(tr("Wymagana kalibracja robota."));
      _pUi->lineEdit_port->setText(this->_pDevice->portName());
      _pUi->label_portLED->setEnabled(true);
      connect(this->_pDevice, SIGNAL(readyRead()), this, SLOT(readFromPort()));
    } else {
      addToLogs(tr("Otwarcie portu nie powiodło się."));
    }
  } else {
    addToLogs(tr("Port jest już otwarty!"));
  }
}

void MainWindow::disconnectSerialPort() {
  if (this->_pDevice->isOpen()) {
    this->_pDevice->close();
    addToLogs(tr("Zamknięto port szeregowy."));
    _pUi->lineEdit_port->setText("");
    _pUi->label_portLED->setEnabled(false);
  } else {
    addToLogs(tr("Port nie jest otwarty!"));
  }
}

void MainWindow::readFromPort() {
  while (this->_pDevice->canReadLine()) {
    this->_RawReadData = this->_pDevice->readLine();
    this->_RawReadData.replace('\x00', "");
    this->_RawReadData.replace('\n', "");
    const char *final_data = this->_RawReadData.data();
    if (parseDataFrame(final_data, this->_DataFrame._Q1deg,
                       this->_DataFrame._Q2mm, this->_DataFrame._Q3deg,
                       this->_DataFrame._GripperState,
                       this->_DataFrame._RedBlocksCount,
                       this->_DataFrame._GreenBlocksCount,
                       this->_DataFrame._BlueBlocksCount,
                       this->_DataFrame._RightLimitSwitch,
                       this->_DataFrame._LeftLimitSwitch,
                       this->_DataFrame._LowerLimitSwitch,
                       this->_DataFrame._UpperLimitSwitch,
                       this->_DataFrame._ForceSensorValue,
                       this->_DataFrame._DetectedColor)) {
      connect(this->_pDevice, SIGNAL(readyRead()), this, SLOT(updateGUI()));
    }
  }
}

void MainWindow::addToLogs(QString Message) {
  QString currentDateTime = QDateTime::currentDateTime().toString("hh:mm:ss");
  _pUi->textEditLogs->append(currentDateTime + ":  " + Message);
}

void MainWindow::updateGUI() {
  _pUi->label_cnt_red->setText(
      QString::number(this->_DataFrame._RedBlocksCount));
  _pUi->label_cnt_green->setText(
      QString::number(this->_DataFrame._GreenBlocksCount));
  _pUi->label_cnt_blue->setText(
      QString::number(this->_DataFrame._BlueBlocksCount));

  if (this->_DataFrame._RightLimitSwitch) {
    _pUi->label_limitswitch_R->setEnabled(true);
  } else {
    _pUi->label_limitswitch_R->setEnabled(false);
  }

  if (this->_DataFrame._LeftLimitSwitch) {
    _pUi->label_limitswitch_L->setEnabled(true);
  } else {
    _pUi->label_limitswitch_L->setEnabled(false);
  }

  if (this->_DataFrame._UpperLimitSwitch) {
    _pUi->label_limitswitch_U->setEnabled(true);
  } else {
    _pUi->label_limitswitch_U->setEnabled(false);
  }

  if (this->_DataFrame._LowerLimitSwitch) {
    _pUi->label_limitswitch_D->setEnabled(true);
  } else {
    _pUi->label_limitswitch_D->setEnabled(false);
  }

  _pUi->lineEdit_Q1->setText(QString::number(this->_DataFrame._Q1deg));
  _pUi->lineEdit_Q2->setText(QString::number(this->_DataFrame._Q2mm));
  _pUi->lineEdit_Q3->setText(QString::number(this->_DataFrame._Q3deg));

  _pUi->lineEdit_X->setText(QString::number(
      117.5 * qSin(this->_DataFrame._Q1deg * M_PI / 180), 'f', 2));
  _pUi->lineEdit_Y->setText(QString::number(
      117.5 * qCos(this->_DataFrame._Q1deg * M_PI / 180), 'f', 2));

  _pUi->lineEdit_Z->setText(QString::number(this->_DataFrame._Q2mm));
  _pUi->lineEdit_fi->setText(QString::number(this->_DataFrame._Q3deg));

  if (this->_DataFrame._GripperState) {
    _pUi->pushButton_CloseGripper->setEnabled(false);
    _pUi->pushButton_OpenGripper->setEnabled(true);
  } else {
    _pUi->pushButton_CloseGripper->setEnabled(true);
    _pUi->pushButton_OpenGripper->setEnabled(false);
  }

  _pUi->progressBar_Force->setValue(this->_DataFrame._ForceSensorValue);

  if (this->_DataFrame._DetectedColor == 114) {
    _pUi->red_color->show();
    _pUi->green_color->hide();
    _pUi->blue_color->hide();
    _pUi->gray_color->hide();
    _pUi->detected_color->setText(tr("Czerwony"));
  } else if (this->_DataFrame._DetectedColor == 103) {
    _pUi->red_color->hide();
    _pUi->green_color->show();
    _pUi->blue_color->hide();
    _pUi->gray_color->hide();
    _pUi->detected_color->setText(tr("Zielony"));
  } else if (this->_DataFrame._DetectedColor == 98) {
    _pUi->red_color->hide();
    _pUi->green_color->hide();
    _pUi->blue_color->show();
    _pUi->gray_color->hide();
    _pUi->detected_color->setText(tr("Niebieski"));
  } else {
    _pUi->red_color->hide();
    _pUi->green_color->hide();
    _pUi->blue_color->hide();
    _pUi->gray_color->show();
    _pUi->detected_color->setText(tr("Brak koloru"));
  }

  Manip.SetQ1_deg(this->_DataFrame._Q1deg);
  Manip.SetQ2_mm(this->_DataFrame._Q2mm);
  Manip.SetQ3_deg(this->_DataFrame._Q3deg);
  _pUi->openGLWidget->update();
}

void MainWindow::on__pAction_Calibration_triggered() {
  this->sendMessageToDevice("C1");
  if (this->_pDevice->isOpen()) {
    _pUi->_pAction_Auto->setEnabled(true);
    _pUi->_pAction_Zigzag->setEnabled(true);
    _pUi->_pAction_Inverse_Kinematics->setEnabled(true);
    _pUi->_pAction_Calibration->setEnabled(false);
  }
}

void MainWindow::on__pAction_Inverse_Kinematics_triggered() {
  _pUi->_pAction_Manual->setEnabled(true);
  _pUi->groupBox_Manual_Control->hide();
  _pUi->groupBox_Inverse_Kinematics->show();
  _pUi->_pAction_Inverse_Kinematics->setEnabled(false);
}

void MainWindow::on__pAction_Manual_triggered() {
  _pUi->_pAction_Manual->setEnabled(false);
  _pUi->groupBox_Inverse_Kinematics->hide();
  _pUi->groupBox_Manual_Control->show();
  _pUi->_pAction_Inverse_Kinematics->setEnabled(true);
}

void MainWindow::on_doubleSpinBox_X_editingFinished() {
  double alfa = 0.0;
  alfa = qAsin(_pUi->doubleSpinBox_X->value() / 117.5);
  double value = _pUi->doubleSpinBox_X->value() / qSin(alfa) * qCos(alfa);
  _pUi->doubleSpinBox_Y->setValue(value);
  _pUi->pushButton_inverse_kinematics_set->setEnabled(true);
}

void MainWindow::on_pushButton_inverse_kinematics_set_clicked() {
  this->sendMessageToDevice(
      "I " + QString::number(_pUi->doubleSpinBox_X->value(), 'f', 2) + " " +
      QString::number(_pUi->doubleSpinBox_Z->value(), 'f', 2) + " " +
      QString::number(_pUi->doubleSpinBox_fi->value()));
  _pUi->pushButton_inverse_kinematics_set->setEnabled(false);
}

void MainWindow::on_pushButton_setSpeed_clicked() {
  this->sendMessageToDevice(
      "S " + QString::number(_pUi->doubleSpinBox_Speed->value()));
  _pUi->pushButton_setSpeed->setEnabled(false);
}

void MainWindow::on_doubleSpinBox_Speed_editingFinished() {
  _pUi->pushButton_setSpeed->setEnabled(true);
}

void MainWindow::on__pAction_Auto_triggered() {
  this->sendMessageToDevice("A1");
  _pUi->_pAction_Reset->setEnabled(true);
  _pUi->_pAction_Auto->setEnabled(false);
  _pUi->_pAction_Zigzag->setEnabled(false);
  addToLogs(tr("UWAGA: Rozpoczynam tryb automatyczny!"));
  addToLogs(tr("Manipulator wykona trzy cykle trybu automatycznego."));
}

void MainWindow::on__pAction_Reset_triggered() {
  this->sendMessageToDevice("A0");
  _pUi->_pAction_Reset->setEnabled(false);
  _pUi->_pAction_Auto->setEnabled(true);
  _pUi->_pAction_Zigzag->setEnabled(true);
  addToLogs(tr("Wykonano reset cykli trybu automatycznego."));
}

void MainWindow::on__pAction_Zigzag_triggered() {
  this->sendMessageToDevice("T1");
  _pUi->_pAction_Reset->setEnabled(true);
  _pUi->_pAction_Zigzag->setEnabled(false);
  _pUi->_pAction_Auto->setEnabled(false);
  addToLogs(tr("UWAGA: Rozpoczynam tryb rysowania trajektorii!"));
}

void MainWindow::on_doubleSpinBox_Z_editingFinished() {
  _pUi->pushButton_inverse_kinematics_set->setEnabled(true);
}

void MainWindow::on__pAction_English_triggered() {
  _pUi->_pAction_Polish->setEnabled(true);
  _pUi->_pAction_English->setEnabled(false);

  if (this->_Translator.load(".qm/ManRTR_en_150.qm")) {
    qApp->installTranslator(&_Translator);
  }
}

void MainWindow::on__pAction_Polish_triggered() {
  _pUi->_pAction_Polish->setEnabled(false);
  _pUi->_pAction_English->setEnabled(true);
  qApp->removeTranslator(&_Translator);
}

void MainWindow::sendMessageToDevice(QString Message) {
  if (this->_pDevice->isOpen() && this->_pDevice->isWritable()) {
    this->_pDevice->write(Message.toStdString().c_str());
  } else {
    addToLogs(tr("Port nie jest otwarty!"));
  }
}

unsigned short int MainWindow::computeCRC16_CCITT_FALSE(const char *pData,
                                                        int Length) {
  short int i;
  unsigned short int wCrc = 0xffff;

  while (Length--) {
    wCrc ^= *(unsigned const char *)pData++ << 8;
    for (i = 0; i < 8; i++)
      wCrc = wCrc & 0x8000 ? (wCrc << 1) ^ 0x1021 : wCrc << 1;
  }
  return wCrc;
}

unsigned short int MainWindow::parseDataFrame(
    const char *pDataFrame, int &Q1deg, int &Q2mm, int &Q3deg,
    int &GripperState, int &RedBlocksCount, int &GreenBlocksCount,
    int &BlueBlocksCount, int &RightLimitSwitch, int &LeftLimitSwitch,
    int &LowerLimitSwitch, int &UpperLimitSwitch, int &ForceSensorValue,
    int &DetectedColor) {
  std::istringstream IStrm(pDataFrame);
  char FHeader;
  unsigned int CRC16;

  IStrm >> FHeader >> Q1deg >> Q2mm >> Q3deg >> GripperState >>
      RedBlocksCount >> GreenBlocksCount >> BlueBlocksCount >>
      RightLimitSwitch >> LeftLimitSwitch >> LowerLimitSwitch >>
      UpperLimitSwitch >> ForceSensorValue >> DetectedColor >> std::hex >>
      CRC16;
  if (IStrm.fail() || FHeader != 'X') return false;
  return static_cast<unsigned short int>(CRC16) ==
         computeCRC16_CCITT_FALSE(pDataFrame, strlen(pDataFrame) - 4);
}

bool MainWindow::parseDataFrame(const std::string &rDataFrame, int &Q1deg,
                                int &Q2mm, int &Q3deg, int &GripperState,
                                int &RedBlocksCount, int &GreenBlocksCount,
                                int &bBlueBlocksCount, int &RightLimitSwitch,
                                int &LeftLimitSwitch, int &LowerLimitSwitch,
                                int &UpperLimitSwitch, int &ForceSensorValue,
                                int &DetectedColor) {
  return parseDataFrame(rDataFrame.c_str(), Q1deg, Q2mm, Q3deg, GripperState,
                        RedBlocksCount, GreenBlocksCount, bBlueBlocksCount,
                        RightLimitSwitch, LeftLimitSwitch, LowerLimitSwitch,
                        UpperLimitSwitch, ForceSensorValue, DetectedColor);
}
