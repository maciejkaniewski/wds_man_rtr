#ifndef MANIPULATOR_HH
#define MANIPULATOR_HH

/**
 * @file
 * @brief The Manipulator class
 *
 * This file contains definition of Minipulator class,
 * which is used to change value (deg or mm) in degrees of freedom.
 */

/**
 * @brief The Manipulator class
 *
 * The class has values of three degrees od freedom.
 * First and third DOF has a value expressed in degree,
 * second in millimeters.
 */
class Manipulator {
  double _Q1deg; /**< Value for first DOF in degrees. */
  double _Q2mm;  /**< Value for second DOF in millimeters.*/
  double _Q3deg; /**< Value for third DOF in degrees.*/

 public:
  /**
   * @brief Manipulator constructor.
   *
   * Creates Manipulator and set all values to 0.
   */
  Manipulator() { _Q1deg = _Q2mm = _Q3deg = 0; }

  /**
   * @brief Sets value in first DOF.
   *
   * Sets degree value in first DOF.
   *
   * @param[in] Q1deg - Value in degrees.
   */
  void SetQ1_deg(double Q1deg) { _Q1deg = Q1deg; }

  /**
   * @brief Gets value of first DOF.
   *
   * Gets value of first DOF in degrees.
   *
   * @return Value of first DOF in degrees.
   */
  double GetQ1_deg() const { return _Q1deg; }

  /**
   * @brief Sets value in second DOF.
   *
   * Sets millimeter value in first DOF.
   *
   * @param[in] Q2mm - Value in millimeters.
   */
  void SetQ2_mm(double Q2mm) { _Q2mm = Q2mm; }

  /**
   * @brief Gets value of second DOF.
   *
   * Gets value of second DOF in millimeters.
   *
   * @return Value of second DOF in millimeters.
   */
  double GetQ2_mm() const { return _Q2mm; }

  /**
   * @brief Sets value in third DOF.
   *
   * Sets degree value in third DOF.
   *
   * @param[in] Q3deg - Value in degrees.
   */
  void SetQ3_deg(double Q3deg) { _Q3deg = Q3deg; }

  /**
   * @brief Gets value of third DOF.
   *
   * Gets value of third DOF in degrees.
   *
   * @return Value of third DOF in degrees.
   */
  double GetQ3_deg() const { return _Q3deg; }
};

/**
 * @brief Extern variable Manip.
 *
 * External variable Manipulator.
 */
extern Manipulator Manip;

#endif  // MANIPULATOR_HH
