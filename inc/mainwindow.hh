#ifndef MAINWINDOW_HH
#define MAINWINDOW_HH

/**
 * @file
 * @brief Definition of MainWindow class
 *
 * This file contains definition of MainWindow class,
 * which is used for viewing graphical interface of application.
 */

#include <QDateTime>
#include <QMainWindow>
#include <QSerialPort>
#include <QTranslator>
#include <QtMath>
#include <bitset>
#include <cassert>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

#include "dialogconnection.hh"
#include "dialoginfo.hh"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

/**
 * @brief The MainWindow class
 *
 * This class is responsible for displaying
 * main GUI of app and handling all buttons
 * and sensor visualisation.
 */
class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  /**
   * @brief Constructor of MainWindow.
   *
   * Constructor of MainWindow.
   *
   * @param[in,out] pParent - Pointer to thr parent.
   */
  MainWindow(QWidget *pParent = nullptr);

  /**
   * @brief Destructor of MainWindow.
   *
   * Destructor of MainWindow.
   */
  ~MainWindow();

  /**
   * @brief Changes event.
   *
   * Translates interface of newly selected language.
   *
   * @param[in] pEvent - Language change event.
   */
  virtual void changeEvent(QEvent *pEvent) override;

 private slots:

  /**
   * @brief Starts Q1 movement in CCW direction.
   *
   * After pressed button starts movement of first DOF in CCW direction.
   */
  void on_pushButton_Q1inc_pressed();

  /**
   * @brief Stops Q1 movement.
   *
   *  After released button stop movement of first DOF in CCW direction.
   */
  void on_pushButton_Q1inc_released();

  /**
   * @brief Starts Q1 movement in CW direction.
   *
   * After pressed button starts movement of first DOF in CW direction.
   */
  void on_pushButton_Q1dec_pressed();

  /**
   * @brief Stops Q1 movement.
   *
   * After released button stop movement of first DOF in CW direction.
   */
  void on_pushButton_Q1dec_released();

  /**
   * @brief Starts Q2 movement in upward direction.
   *
   * After pressed button starts movement of second DOF in upward direction.
   */
  void on_pushButton_Q2dec_pressed();

  /**
   * @brief Stops Q2 movement.
   *
   * After released button stop movement of second DOF in upward direction.
   */
  void on_pushButton_Q2dec_released();

  /**
   * @brief Starts Q2 movement in downward direction.
   *
   * After pressed button starts movement of second DOF in downward direction.
   */
  void on_pushButton_Q2inc_pressed();

  /**
   * @brief Stops Q2 movement.
   *
   * After released button stop movement of second DOF in downward direction.
   */
  void on_pushButton_Q2inc_released();

  /**
   * @brief Starts Q3 movement in CCW direction.
   *
   * After pressed button starts movement of third DOF in CCW direction.
   */
  void on_pushButton_Q3inc_pressed();

  /**
   * @brief Stops Q3 movement.
   *
   * After released button stop movement of third DOF in CCW direction.
   */
  void on_pushButton_Q3inc_released();

  /**
   * @brief Starts Q3 movement in CW direction.
   *
   * After pressed button starts movement of third DOF in CW direction.
   */
  void on_pushButton_Q3dec_pressed();

  /**
   * @brief Stops Q3 movement.
   *
   * After released button stop movement of third DOF in CW direction.
   */
  void on_pushButton_Q3dec_released();

  /**
   * @brief Closes manipulator's gripper.
   *
   * After click closes manipulator's gripper.
   */
  void on_pushButton_CloseGripper_clicked();

  /**
   * @brief Opens manipulator's gripper.
   *
   * After click opens manipulator's gripper.
   */
  void on_pushButton_OpenGripper_clicked();

  /**
   * @brief Opens connection dialog window.
   *
   * Opens connection dialog window.
   */
  void on__pAction_Connection_triggered();

  /**
   * @brief Opens info dialog window.
   *
   * Opens info dialog window.
   */
  void on__pAction_Info_triggered();

  /**
   * @brief Connects to the device.
   *
   * After getting signal connects to the device.
   *
   * @param[in] PortName - Device's port name.
   */
  void connectSerialPort(QString PortName);

  /**
   * @brief Disconnect from the device.
   *
   * After getting signal disconnect from the device.
   */
  void disconnectSerialPort();

  /**
   * @brief Reads data from opened port.
   *
   * Reads data from opened port.
   */
  void readFromPort();

  /**
   * @brief Adds message to the log field.
   *
   * After getting signal adds message to the log field.
   *
   * @param[in] Message - Message to be added.
   */
  void addToLogs(QString Message);

  /**
   * @brief Updates displayed data.
   *
   * Updates displayed data.
   */
  void updateGUI();

  /**
   * @brief Sends messsgae to calibrate manipulator.
   *
   * Sends messsgae to calibrate manipulator.
   */
  void on__pAction_Calibration_triggered();

  /**
   * @brief Switches to Inverse Kinematics mode.
   *
   * Switches to Inverse Kinematics mode.
   */
  void on__pAction_Inverse_Kinematics_triggered();

  /**
   * @brief Switches to Manual mode.
   *
   * Switches to Manual mode.
   */
  void on__pAction_Manual_triggered();

  /**
   * @brief Performs operations when X cooridante editing finished.
   *
   * Performs operations when X cooridante editing finished.
   */
  void on_doubleSpinBox_X_editingFinished();

  /**
   * @brief Sets and sends data adjusted in Inverse Kinematics mode.
   *
   * After click sets and sends data adjusted in Inverse Kinematics mode.
   */
  void on_pushButton_inverse_kinematics_set_clicked();

  /**
   * @brief Sets and sends adjusted speed.
   *
   * After click sets and sends adjusted speed.
   */
  void on_pushButton_setSpeed_clicked();

  /**
   * @brief Performs operations when speed value editing finished.
   *
   * Performs operations when speed value editing finished.
   */
  void on_doubleSpinBox_Speed_editingFinished();

  /**
   * @brief Executes Automatic Mode.
   *
   * Executes Automatic Mode.
   */
  void on__pAction_Auto_triggered();

  /**
   * @brief Resets Automatic and Trajectory mode.
   *
   * Resets Automatic and Trajectory mode.
   */
  void on__pAction_Reset_triggered();

  /**
   * @brief Executes trajectory drawing mode.
   *
   * Executes trajectory drawing mode.
   */
  void on__pAction_Zigzag_triggered();

  /**
   * @brief Performs operations when Z cooridante editing finished.
   *
   * Performs operations when Z cooridante editing finished.
   */
  void on_doubleSpinBox_Z_editingFinished();

  /**
   * @brief Change language to english.
   *
   * Change language to english.
   */
  void on__pAction_English_triggered();

  /**
   * @brief Change language to polish.
   */
  void on__pAction_Polish_triggered();

 private:
  /**
   * @brief _pUi User interface handler.
   */
  Ui::MainWindow *_pUi;

  /**
   * @brief Serial port handler.
   */
  QSerialPort *_pDevice = nullptr;

  /**
   * @brief Connection dialog window handler.
   */
  DialogConnection *_pDialogConnection;

  /**
   * @brief Info dialog window handler.
   */
  DialogInfo *_pDialogInfo;

  /**
   * @brief _Translator Translator instance.
   */
  QTranslator _Translator;

  /**
   * @brief Raw data from the device.
   */
  QByteArray _RawReadData;

  /**
   * @brief Frame to control manipulator.
   */
  QString _ManualControlFrame = "M 0 0 0";

  /**
   * @brief Structure of data frame.
   *
   * Data frame which contains all data form manipulator's sensors.
   *
   */
  typedef struct DataFrame {
    int _Q1deg;            /**< Position of the first joint in degrees. */
    int _Q2mm;             /**< Position of the second joint in milimeters. */
    int _Q3deg;            /**< Position of the third joint in degrees. */
    int _GripperState;     /**< Gripper state (0 - closed, 1 - open). */
    int _RedBlocksCount;   /**< The number of red blocks detected by the digital
                              distance sensor. */
    int _GreenBlocksCount; /**< The number of green blocks detected by the
                              digital distance sensor. */
    int _BlueBlocksCount; /**< The number of blue blocks detected by the digital
                             distance sensor. */
    int _RightLimitSwitch; /**< State of the right limit switch, not
                            * activated(0) or activated(1).*/
    int _LeftLimitSwitch;  /**< State of the left limit switch, not
                            * activated(0) or activated(1).*/
    int _LowerLimitSwitch; /**< State of the lower limit switch, not
                            * activated(0) or activated(1).*/
    int _UpperLimitSwitch; /**< State of the upper limit switch, not
                            * activated(0) or activated(1).*/
    int _ForceSensorValue; /**< The value returned by the force sensor [0:4096].
                            */
    int _DetectedColor;    /**< Color detected by the color sensor, red(114),
                            * green(103), blue(98) or none(110). */

  } DataFrame;

  /**
   * @brief Variable for storing parsed data.
   */
  DataFrame _DataFrame;

  /**
   * @brief Sends message to STM32.
   *
   * Sends message to STM32.
   *
   * @param[in] Message - The message to sent.
   */
  void sendMessageToDevice(QString Message);

  /**
   * @brief Computes CRC.
   *
   * Computes CRC value with CRC16_CCITT_FALSE algorithm.
   *
   * @param[in] pData - Data frame.
   * @param[in] Length - Length of data frame.
   * @return CRC value.
   */
  unsigned short int computeCRC16_CCITT_FALSE(const char *pData, int Length);

  /**
   * @brief Parses data frame.
   *
   * Parsed data frame into structure.
   *
   * @param[in] pDataFrame - Data frame.
   * @param[out] Q1deg -  Position of the first joint in degrees.
   * @param[out] Q2mm - Position of the second joint in milimeters.
   * @param[out] Q3deg - Position of the third joint in degrees.
   * @param[out] GripperState - Gripper state (0 - closed, 1 - open).
   * @param[out] RedBlocksCount - The number of red blocks detected by the
   * digital distance sensor.
   * @param[out] GreenBlocksCount - The number of green blocks detected by the
   * digital distance sensor.
   * @param[out] BlueBlocksCount - The number of blue blocks detected by the
   * digital distance sensor.
   * @param[out] RightLimitSwitch - State of the right limit switch, not
   * activated(0) or activated(1).
   * @param[out] LeftLimitSwitch - State of the left limit switch, not
   * activated(0) or activated(1).
   * @param[out] LowerLimitSwitch - State of the lower limit switch, not
   * activated(0) or activated(1).
   * @param[out] UpperLimitSwitch - State of the upper limit switch, not
   * activated(0) or activated(1).
   * @param[out] ForceSensorValue - The value returned by the force sensor
   * [0:4096].
   * @param[out] DetectedColor - Color detected by the color sensor, red(114),
   * green(103), blue(98) or none(110).
   * @retval 0 - Input stream failed or incorrect data frame header.
   * @retval !0 - Successful CRC caluclation.
   */
  unsigned short int parseDataFrame(const char *pDataFrame, int &Q1deg,
                                    int &Q2mm, int &Q3deg, int &GripperState,
                                    int &RedBlocksCount, int &GreenBlocksCount,
                                    int &BlueBlocksCount, int &RightLimitSwitch,
                                    int &LeftLimitSwitch, int &LowerLimitSwitch,
                                    int &UpperLimitSwitch,
                                    int &ForceSensorValue, int &DetectedColor);

  /**
   * @brief Checks data frame.
   *
   * Checks if data frame is correct.
   *
   * @param[in] rDataFrame - Data frame.
   * @param[out] Q1deg - Position of the first joint in degrees.
   * @param[out] Q2mm - Position of the second joint in milimeters.
   * @param[out] Q3deg - Position of the third joint in degrees.
   * @param[out] GripperState - Gripper state, open (0) or closed (1).
   * @param[out] RedBlocksCount - The number of red blocks detected by the
   * digital distance sensor.
   * @param[out] GreenBlocksCount - The number of green blocks detected by the
   * digital distance sensor.
   * @param[out] BlueBlocksCount - The number of blue blocks detected by the
   * digital distance sensor.
   * @param[out] RightLimitSwitch - State of the right limit switch, not
   * activated(0) or activated(1).
   * @param[out] LeftLimitSwitch - State of the left limit switch, not
   * activated(0) or activated(1).
   * @param[out] LowerLimitSwitch - State of the lower limit switch, not
   * activated(0) or activated(1).
   * @param[out] UpperLimitSwitch - State of the upper limit switch, not
   * activated(0) or activated(1).
   * @param[out] ForceSensorValue - The value returned by the force sensor
   * [0:4096].
   * @param[out] DetectedColor - Color detected by the color sensor, red(114),
   * green(103), blue(98) or none(110).
   * @retval true - Data frame is correct.
   * @retval false - Otherwise.
   */
  bool parseDataFrame(const std::string &rDataFrame, int &Q1deg, int &Q2mm,
                      int &Q3deg, int &GripperState, int &RedBlocksCount,
                      int &GreenBlocksCount, int &BlueBlocksCount,
                      int &RightLimitSwitch, int &LeftLimitSwitch,
                      int &LowerLimitSwitch, int &UpperLimitSwitch,
                      int &ForceSensorValue, int &DetectedColor);
};
#endif  // MAINWINDOW_HH
