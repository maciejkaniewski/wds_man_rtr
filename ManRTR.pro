QT       += core gui serialport opengl xml
LIBS+=-lQGLViewer-qt5 -lSOIL

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

copydata.commands = $(COPY_DIR) $$PWD/textures $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

SOURCES += \
    src/dialogconnection.cpp \
    src/dialoginfo.cpp \
    src/main.cpp \
    src/mainwindow.cpp \
    src/manipulator.cpp \
    src/viewer.cpp

HEADERS += \
    inc/dialogconnection.hh \
    inc/dialoginfo.hh \
    inc/mainwindow.hh \
    inc/manipulator.hh \
    inc/viewer.hh

FORMS += \
    ui/dialogconnection.ui \
    ui/dialoginfo.ui \
    ui/mainwindow.ui

TRANSLATIONS += \
    ManRTR_en_150.ts
CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH+=inc
INCLUDEPATH+=src
INCLUDEPATH+=ui

RESOURCES += \
    res/resources.qrc


DISTFILES +=
