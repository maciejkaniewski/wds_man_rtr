#include "viewer.hh"

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <SOIL/SOIL.h>

#include "manipulator.hh"
static GLuint texture_background;
static GLuint texture_wood;
static GLuint texture_gray;
static GLuint texture_manipulator;
static GLuint texture_steel;
static GLuint texture_thread;
static GLuint texture_black;

Viewer::Viewer(QWidget* pParent) : QGLViewer(pParent) {}

void Viewer::GLLoadTexture(GLuint* pTexture, const std::string& rPath) {
  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  glGenTextures(1, pTexture);
  glBindTexture(GL_TEXTURE_2D, *pTexture);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  int width, height;
  unsigned char* image =
      SOIL_load_image(rPath.c_str(), &width, &height, 0, SOIL_LOAD_RGB);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
               GL_UNSIGNED_BYTE, image);
}

void Viewer::GLCreateBox(double SizeX, double SizeY, double SizeZ) {
  glPushMatrix();
  glScalef(SizeX, SizeY, SizeZ);

  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.5, -0.5, 0.5);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.5, 0.5, 0.5);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(-0.5, 0.5, 0.5);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(-0.5, -0.5, 0.5);
  glEnd();

  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.5, -0.5, -0.5);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.5, 0.5, -0.5);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(-0.5, 0.5, -0.5);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(-0.5, -0.5, -0.5);
  glEnd();

  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.5, -0.5, -0.5);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.5, 0.5, -0.5);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(0.5, 0.5, 0.5);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(0.5, -0.5, 0.5);
  glEnd();

  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(-0.5, -0.5, 0.5);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(-0.5, 0.5, 0.5);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(-0.5, 0.5, -0.5);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(-0.5, -0.5, -0.5);
  glEnd();

  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.5, 0.5, 0.5);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.5, 0.5, -0.5);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(-0.5, 0.5, -0.5);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(-0.5, 0.5, 0.5);
  glEnd();

  glBegin(GL_POLYGON);
  glTexCoord2f(0.0, 0.0);
  glVertex3f(0.5, -0.5, -0.5);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(0.5, -0.5, 0.5);
  glTexCoord2f(1.0, 1.0);
  glVertex3f(-0.5, -0.5, 0.5);
  glTexCoord2f(1.0, 0.0);
  glVertex3f(-0.5, -0.5, -0.5);
  glEnd();

  glPopMatrix();
}

void Viewer::GLCreateCylinder(GLfloat Radius, GLfloat Height) {
  double i, resolution = 0.001;

  glPushMatrix();
  glRotatef(90, 1, 0, 0);

  glBegin(GL_TRIANGLE_FAN);
  glTexCoord2f(0.5, 0.5);
  glVertex3f(0, Height, 0);
  for (i = 2 * M_PI; i >= 0; i -= resolution)

  {
    glTexCoord2f(0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f);
    glVertex3f(Radius * cos(i), Height, Radius * sin(i));
  }

  glTexCoord2f(0.5, 0.5);
  glVertex3f(Radius, Height, 0);
  glEnd();

  glBegin(GL_TRIANGLE_FAN);
  glTexCoord2f(0.5, 0.5);
  glVertex3f(0, 0, 0); /* center */
  for (i = 0; i <= 2 * M_PI; i += resolution) {
    glTexCoord2f(0.5f * cos(i) + 0.5f, 0.5f * sin(i) + 0.5f);
    glVertex3f(Radius * cos(i), 0, Radius * sin(i));
  }
  glEnd();

  glBegin(GL_QUAD_STRIP);
  for (i = 0; i <= 2 * M_PI; i += resolution) {
    const float tc = (i / (float)(2 * M_PI));
    glTexCoord2f(tc, 0.0);
    glVertex3f(Radius * cos(i), 0, Radius * sin(i));
    glTexCoord2f(tc, 1.0);
    glVertex3f(Radius * cos(i), Height, Radius * sin(i));
  }

  glTexCoord2f(0.0, 0.0);
  glVertex3f(Radius, 0, 0);
  glTexCoord2f(0.0, 1.0);
  glVertex3f(Radius, Height, 0);
  glEnd();

  glPopMatrix();
}

void Viewer::draw() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  /*-----------------------------------------------------
   *  Tworzenie tła wypełnionego wygenerowaną teksturą
   */

  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
  glBindTexture(GL_TEXTURE_2D, texture_background);

  glMatrixMode(GL_MODELVIEW);
  glPushMatrix();

  glLoadIdentity();
  glDepthMask(GL_FALSE);

  glMatrixMode(GL_PROJECTION);
  glPushMatrix();

  glLoadIdentity();
  glOrtho(0, 1, 1, 0, -1, 1);

  glBegin(GL_QUADS);   // Tworzenie kwadratu, na którym będzie
  glTexCoord2f(0, 0);  // rozpięta tekstura tła.
  glVertex2f(0, 0);

  glTexCoord2f(1, 0);
  glVertex2f(1, 0);

  glTexCoord2f(1, 1);
  glVertex2f(1, 1);

  glTexCoord2f(0, 1);
  glVertex2f(0, 1);
  glEnd();

  glMatrixMode(GL_PROJECTION);
  glPopMatrix();

  glMatrixMode(GL_MODELVIEW);
  glPopMatrix();
  glDepthMask(GL_TRUE);

  /*- Koniec ---
   *  Tworzenie tła wypełnionego
   *------------------------------------------------------------*/

  // Tworzenie stołu
  glBindTexture(GL_TEXTURE_2D, texture_wood);
  glPushMatrix();
  glTranslatef(0.0, 0.0, -0.15 / 2);
  GLCreateBox(2.15, 2.35, 0.15);
  glPopMatrix();

  // Tworzenie podstwy okrągłej
  glBindTexture(GL_TEXTURE_2D, texture_manipulator);
  glPushMatrix();
  glTranslatef(-0.2, 0.0, 0.0);
  GLCreateCylinder(0.35, 0.20);
  glPopMatrix();

  // Tworznie prostokąta do podstawy
  glPushMatrix();
  glTranslatef(-0.55 / 2 - 0.2, 0.0, 0.20 / 2 - 0.0001);
  GLCreateBox(0.65, 0.7 - 0.01, 0.20 - 0.0001);
  glPopMatrix();

  // Tworzenie silnika dla Q1
  glBindTexture(GL_TEXTURE_2D, texture_gray);
  glPushMatrix();
  glTranslatef(-0.5 - 0.2, 0, 0.20);
  GLCreateCylinder(0.075, 0.05);
  glPopMatrix();

  glBindTexture(GL_TEXTURE_2D, texture_steel);
  glPushMatrix();
  glTranslatef(-0.5 - 0.2, 0, 0.20 + 0.075 / 2);
  GLCreateCylinder(0.07, 0.125);
  glPopMatrix();

  glBindTexture(GL_TEXTURE_2D, texture_black);
  glPushMatrix();
  glTranslatef(-0.5 - 0.2, 0, 0.20 + 0.13 + 0.065 / 2);
  GLCreateCylinder(0.07, 0.06);
  glPopMatrix();

  glTranslatef(-0.20, 0.0, 0.25 + 0.15 / 8);
  glRotatef(Manip.GetQ1_deg(), 0.0, 0.0, 1.0);

  // Tworzenie silnika dla Q2
  glBindTexture(GL_TEXTURE_2D, texture_gray);
  glPushMatrix();
  glTranslatef(0, 0, 0.20 + 1 + 0.080 / 2);
  GLCreateCylinder(0.075, 0.05);
  glPopMatrix();

  glBindTexture(GL_TEXTURE_2D, texture_steel);
  glPushMatrix();
  glTranslatef(-0, 0, 0.20 + 0.075 / 2 + 1 + 0.080 / 2);
  GLCreateCylinder(0.07, 0.125);
  glPopMatrix();

  glBindTexture(GL_TEXTURE_2D, texture_black);
  glPushMatrix();
  glTranslatef(0, 0, 0.20 + 0.13 + 0.065 / 2 + 1 + 0.080 / 2);
  GLCreateCylinder(0.07, 0.06);
  glPopMatrix();

  glBindTexture(GL_TEXTURE_2D, texture_manipulator);
  // Tworzenie dolnej podstawy (mniejszy dolny cylinder)
  glPushMatrix();
  glTranslatef(0.0, 0.0, -0.15 / 2);
  GLCreateCylinder(0.20, 0.15 / 2);
  glPopMatrix();

  // Tworzenie dolnej podstawy (dolny cylinder)
  glPushMatrix();
  glTranslatef(0.0, 0.0, -0.15 / 4);
  GLCreateCylinder(0.30, 0.20 / 2);
  glPopMatrix();

  // Tworzenie talerza
  glPushMatrix();
  glTranslatef(0.0, 0.0, 0.175);
  GLCreateCylinder(0.30, 0.20 / 2);
  glPopMatrix();

  // Tworzenie śruby pociągowej
  glBindTexture(GL_TEXTURE_2D, texture_thread);
  glPushMatrix();
  glTranslatef(0.0, 0.0, 0.15 / 4);
  glRotatef(Manip.GetQ2_mm() * 360 / 8, 0.0, 0.0, 1.0);
  GLCreateCylinder(0.02, 1);
  glPopMatrix();

  // Tworzenie 1 preta
  glBindTexture(GL_TEXTURE_2D, texture_steel);
  glPushMatrix();
  glTranslatef(0.2 * cos(M_PI), 0.2 * sin(M_PI), 0.15 / 4);
  GLCreateCylinder(0.02, 1);
  glPopMatrix();

  // Tworzenie 2 preta
  glPushMatrix();
  glTranslatef(0.2 * cos(M_PI / 3), 0.2 * sin(M_PI / 3), 0.15 / 4);
  GLCreateCylinder(0.02, 1);
  glPopMatrix();

  // Tworzenie 3 preta
  glPushMatrix();
  glTranslatef(0.2 * cos(-M_PI / 3), 0.2 * sin(-M_PI / 3), 0.15 / 4);
  GLCreateCylinder(0.02, 1);
  glPopMatrix();

  // Tworzenie gornej podstawy (gorny cylinder)
  glBindTexture(GL_TEXTURE_2D, texture_manipulator);
  glPushMatrix();
  glTranslatef(0, 0, 1);
  GLCreateCylinder(0.30, 0.25);
  glPopMatrix();

  // Tworzenie ramienia robota

  // Tworzenie 1 cylindra ramienia
  glPushMatrix();
  glTranslatef(0, 0, ((Manip.GetQ2_mm()) - 55) * 0.4725 / 115.0);
  glTranslatef(0, 0, 0.585 / 2);  // 0585/2
  GLCreateCylinder(0.3, 0.25);

  // Tworzenie prostopadloscianu ramienia
  glTranslatef(0.5, 0, 0.25 / 2);
  GLCreateBox(0.75, 0.40, 0.25 - 0.001);

  // Tworzenie 2 cylindra ramienia
  glTranslatef(0.25 + 0.25 / 2, 0, -0.25 / 2);
  GLCreateCylinder(0.20, 0.25);
  glPopMatrix();

  // Tworzenie chwytaka
  glBindTexture(GL_TEXTURE_2D, texture_gray);
  glPushMatrix();
  glTranslatef(0, 0, ((Manip.GetQ2_mm()) - 55) * 0.4725 / 115.0);
  glTranslatef(0.75 + 0.25 / 2, 0, 0.585 / 2 - 0.025);
  glRotatef(Manip.GetQ3_deg(), 0.0, 0.0, 1.0);
  GLCreateBox(0.1, 0.1, 0.10);

  glBindTexture(GL_TEXTURE_2D, texture_manipulator);
  glTranslatef(0, 0, -0.10);
  GLCreateCylinder(0.20, 0.05);

  glBindTexture(GL_TEXTURE_2D, texture_gray);
  glTranslatef(0, 0.15, -0.15);
  GLCreateBox(0.1, 0.075, 0.30);

  glTranslatef(0, -0.30, 0);
  GLCreateBox(0.1, 0.075, 0.30);
  glPopMatrix();

  glDisable(GL_TEXTURE_2D);
  glFlush();
}

void Viewer::init() {
  restoreStateFromFile();
  GLfloat lightPosition[] = {1.0, 1.0, 1.0, 0.0};
  glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glClearColor(1, 1, 1, 0.0);
  glShadeModel(GL_FLAT);
  glEnable(GL_DEPTH_TEST);

  texture_background = SOIL_load_OGL_texture(
      "textures/background.png", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
      SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB |
          SOIL_FLAG_COMPRESS_TO_DXT);

  GLLoadTexture(&texture_wood, "textures/wood.jpg");
  GLLoadTexture(&texture_gray, "textures/gray.jpg");
  GLLoadTexture(&texture_manipulator, "textures/yellow.jpg");
  GLLoadTexture(&texture_steel, "textures/steel.jpg");
  GLLoadTexture(&texture_thread, "textures/thread.jpg");
  GLLoadTexture(&texture_black, "textures/black.jpg");

  // Opens help window
  // help();
}
