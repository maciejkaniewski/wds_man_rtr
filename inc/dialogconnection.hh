#ifndef DIALOGCONNECTION_HH
#define DIALOGCONNECTION_HH

/**
 * @file
 * @brief Definition of DialogConnection class
 *
 * This file contains definition of DialogConnection class,
 * which is used for connection between manipulator and application.
 */
#include <QDialog>
#include <QSerialPort>

namespace Ui {
class DialogConnection;
}

/**
 * @brief The DialogConnection class
 *
 * This class is used for creating seperated window to manage connection with
 * manipulator. This class is connected to the MainWindow by signals.
 */
class DialogConnection : public QDialog {
  Q_OBJECT

 public:
  /**
   * @brief Constructor of DialogConnection.
   *
   * Constructor of DialogConnection.
   *
   * @param[in,out] pParent - Pointer to the parent.
   */
  explicit DialogConnection(QWidget *pParent = nullptr);

  /**
   * @brief Destructor of DialogConnection.
   *
   * Destructor of DialogConnection.
   */
  ~DialogConnection();
 signals:

  /**
   * @brief Sends signal to pass serial port name.
   *
   * Sends signal to pass serial port name to the MainWindow.
   *
   * @param[in] SerialPortName - Serial port name to be send.
   */
  void sendSerialPortName(QString SerialPortName);

  /**
   * @brief Sends signal to pass message to logs.
   *
   * Sends signal to pass message to logs in MainWindow.
   *
   * @param[in] Message - Message to be passed.
   */
  void sendToLogs(QString Message);

  /**
   * @brief Sends signal to disconnect from the device.
   *
   * Sends signal to disconnect from the device.
   */
  void sendDisconnect();

 private slots:

  /**
   * @brief Searches for available devices.
   *
   * After click searches for available devices and list them in .
   *
   */
  void on_pushButton_search_clicked();

  /**
   * @brief Disconnects from the device.
   *
   * After click disconnects from the device
   */
  void on_pushButton_disconnect_clicked();

  /**
   * @brief Connects to the device.
   *
   * After click connects to the device.
   */
  void on_pushButton_connect_clicked();

 private:
  /**
   * @brief Graphical interface of dialog.
   */
  Ui::DialogConnection *_pUi;
};

#endif  // DIALOGCONNECTION_HH
