#include "dialoginfo.hh"

#include "ui_dialoginfo.h"

DialogInfo::DialogInfo(QWidget *parent)
    : QDialog(parent), _pUi(new Ui::DialogInfo) {
  setWindowFlags(Qt::Window | Qt::WindowCloseButtonHint);
  _pUi->setupUi(this);
}

DialogInfo::~DialogInfo() { delete _pUi; }
