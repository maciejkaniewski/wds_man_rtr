#ifndef DIALOGINFO_HH
#define DIALOGINFO_HH

/**
 * @file
 * @brief Definition of DialogInfo class
 *
 * This file contains definition of DialogInfo class,
 * which is used for display information about application.
 */
#include <QDialog>

namespace Ui {
class DialogInfo;
}
/**
 * @brief The DialogInfo class
 *
 * This class is used for creating seperated window to display information about
 * application.
 */
class DialogInfo : public QDialog {
  Q_OBJECT

 public:
  /**
   * @brief Constructor of DialogInfo.
   *
   * Constructor of DialogInfo.
   *
   * @param[in,out] pParent - Pointer to the parent.
   */
  explicit DialogInfo(QWidget *pParent = nullptr);

  /**
   * @brief Destructor of DialogInfo.
   *
   * Destructor of DialogInfo.
   */
  ~DialogInfo();

 private:
  /**
   * @brief Graphical interface of dialog.
   */
  Ui::DialogInfo *_pUi;
};

#endif  // DIALOGINFO_HH
