<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_150">
<context>
    <name>DialogConnection</name>
    <message>
        <location filename="ui/dialogconnection.ui" line="20"/>
        <source>Połączenie</source>
        <translation>Connection</translation>
    </message>
    <message>
        <location filename="ui/dialogconnection.ui" line="43"/>
        <source>Szukaj</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="ui/dialogconnection.ui" line="50"/>
        <source>Rozłącz</source>
        <translation>Disconnect</translation>
    </message>
    <message>
        <location filename="ui/dialogconnection.ui" line="64"/>
        <source>Połącz</source>
        <translation>Connect</translation>
    </message>
    <message>
        <location filename="src/dialogconnection.cpp" line="15"/>
        <source>Znalezione urządzenia:</source>
        <translation>Found devices:</translation>
    </message>
</context>
<context>
    <name>DialogInfo</name>
    <message>
        <location filename="ui/dialoginfo.ui" line="29"/>
        <source>Informacje</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="ui/dialoginfo.ui" line="57"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:16px; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Aplikacja została stworzona do sterowania fizycznym Manipulatorem &lt;br /&gt;w ramach kursów Wizualizacja Danych Sensorycznych i Sterowniki Robotów.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Autorzy:&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Maciej Kaniewski&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Dawid Rogaliński&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;meta charset=&quot;utf-8&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Ubuntu&apos;; font-size:16px; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:10pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;The application was created to control the real one Manipulator as part of the Sensor Data Visualization and Robot Controllers courses.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:12pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Authors:&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Maciej Kaniewski&lt;/span&gt;&lt;/p&gt;
&lt;p align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:12pt;&quot;&gt;Dawid Rogaliński&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="ui/mainwindow.ui" line="38"/>
        <source>Manipulator</source>
        <translation>Manipulator</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3366"/>
        <source>OpenGL</source>
        <translation>OpenGL</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="171"/>
        <source>Czujnik nacisku</source>
        <translation>Force sensor</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="267"/>
        <source>Wyłączniki krańcowe</source>
        <translation>Limit switches</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="342"/>
        <source>Dolny</source>
        <translation>Bottom</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="311"/>
        <source>Górny</source>
        <translation>Top</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="500"/>
        <source>Prawy</source>
        <translation>Right</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="438"/>
        <source>Lewy</source>
        <translation>Left</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="611"/>
        <source>Czujnik kolorów</source>
        <translation>Color sensor</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="810"/>
        <location filename="src/mainwindow.cpp" line="259"/>
        <source>Brak koloru</source>
        <translation>No color</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="877"/>
        <source>Licznik kolorów</source>
        <translation>Color counter</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="942"/>
        <location filename="src/mainwindow.cpp" line="241"/>
        <source>Czerwony</source>
        <translation>Red</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1000"/>
        <location filename="src/mainwindow.cpp" line="247"/>
        <source>Zielony</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1069"/>
        <location filename="ui/mainwindow.ui" line="1180"/>
        <location filename="ui/mainwindow.ui" line="1251"/>
        <source>0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1112"/>
        <location filename="src/mainwindow.cpp" line="253"/>
        <source>Niebieski</source>
        <translation>Blue</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1345"/>
        <source>Sterowanie ręczne</source>
        <translation>Manual control</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1379"/>
        <location filename="ui/mainwindow.ui" line="2025"/>
        <source>Aktualna pozycja</source>
        <translation>Current position</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1406"/>
        <source>Zmień pozycję</source>
        <translation>Change position</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1513"/>
        <source>Q1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1528"/>
        <location filename="ui/mainwindow.ui" line="2095"/>
        <location filename="ui/mainwindow.ui" line="2228"/>
        <location filename="ui/mainwindow.ui" line="2243"/>
        <location filename="ui/mainwindow.ui" line="2417"/>
        <location filename="ui/mainwindow.ui" line="2432"/>
        <location filename="ui/mainwindow.ui" line="2447"/>
        <source>mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1589"/>
        <source>Q3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1604"/>
        <location filename="ui/mainwindow.ui" line="1619"/>
        <location filename="ui/mainwindow.ui" line="2390"/>
        <location filename="ui/mainwindow.ui" line="2462"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1646"/>
        <source>Q2:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1715"/>
        <location filename="ui/mainwindow.ui" line="1867"/>
        <location filename="ui/mainwindow.ui" line="1905"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1753"/>
        <location filename="ui/mainwindow.ui" line="1791"/>
        <location filename="ui/mainwindow.ui" line="1829"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="1953"/>
        <location filename="ui/mainwindow.ui" line="3519"/>
        <location filename="ui/mainwindow.ui" line="3522"/>
        <source>Kinematyka odwrotna</source>
        <translation>Inverse kinematics</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2002"/>
        <location filename="ui/mainwindow.ui" line="3017"/>
        <source>Zastosuj</source>
        <translation>Apply</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2052"/>
        <source>Zadaj pozycję</source>
        <translation>Set position</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2122"/>
        <source>Y:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2186"/>
        <source>X:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2213"/>
        <source>Z:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2375"/>
        <source>φ:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2692"/>
        <source>Chwytak</source>
        <translation>Gripper</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2756"/>
        <source>Zamknij</source>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2800"/>
        <source>Otwórz</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2881"/>
        <source>Prędkość</source>
        <translation>Speed</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="2960"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3081"/>
        <source>Status połączenia</source>
        <translation>Connection status</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3127"/>
        <source>Port:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3273"/>
        <source>Logi</source>
        <translation>Logs</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3392"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3438"/>
        <location filename="ui/mainwindow.ui" line="3441"/>
        <source>Połączenie</source>
        <translation>Connection</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3444"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3456"/>
        <location filename="ui/mainwindow.ui" line="3459"/>
        <source>Kalibracja</source>
        <translation>Calbration</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3462"/>
        <source>F2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3474"/>
        <source>Manual</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3477"/>
        <source>Tryb ręczny</source>
        <translation>Manual control</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3480"/>
        <source>F3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3492"/>
        <source>Auto</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3495"/>
        <source>Tryb automatyczny</source>
        <translation>Automatic mode</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3504"/>
        <source>Info</source>
        <translation>Info</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3507"/>
        <source>Informacje</source>
        <translation>Information</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3534"/>
        <source>Reset</source>
        <translation></translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3537"/>
        <source>Reset trybu automatycznego</source>
        <translation>Automatic mode reset</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3549"/>
        <source>Zygzak</source>
        <translation>Zigzag</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3552"/>
        <source>Rysowanie trajektorii</source>
        <translation>Drawing a trajectory</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3571"/>
        <location filename="ui/mainwindow.ui" line="3593"/>
        <source>Zmien język</source>
        <translation>Change language</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3574"/>
        <source>Zmień język na angielski</source>
        <translation>Change the language to English</translation>
    </message>
    <message>
        <location filename="ui/mainwindow.ui" line="3596"/>
        <source>Zmień język na polski</source>
        <translation>Change the language to Polish</translation>
    </message>
    <message>
        <source>Zmiana języka eng</source>
        <translation type="vanished">Change language</translation>
    </message>
    <message>
        <source>Zmiana języka pl</source>
        <translation type="vanished">Change language</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="130"/>
        <source>Otwarto port szeregowy.</source>
        <translation>The serial port was opened.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="131"/>
        <source>Wymagana kalibracja robota.</source>
        <translation>Robot calibration required.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="136"/>
        <source>Otwarcie portu nie powiodło się.</source>
        <translation>Port opening was not successful.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="139"/>
        <source>Port jest już otwarty!</source>
        <translation>The port is already opened!</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="146"/>
        <source>Zamknięto port szeregowy.</source>
        <translation>The serial port has been closed.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="150"/>
        <location filename="src/mainwindow.cpp" line="366"/>
        <source>Port nie jest otwarty!</source>
        <translation>The port is not open!</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="323"/>
        <source>UWAGA: Rozpoczynam tryb automatyczny!</source>
        <translation>WARNING: I am starting automatic mode!</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="324"/>
        <source>Manipulator wykona trzy cykle trybu automatycznego.</source>
        <translation>The manipulator will perform three cycles of automatic mode.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="332"/>
        <source>Wykonano reset cykli trybu automatycznego.</source>
        <translation>The automatic mode cycles have been reset.</translation>
    </message>
    <message>
        <location filename="src/mainwindow.cpp" line="340"/>
        <source>UWAGA: Rozpoczynam tryb rysowania trajektorii!</source>
        <translation>WARNING: I am starting the trajectory drawing mode!</translation>
    </message>
</context>
</TS>
