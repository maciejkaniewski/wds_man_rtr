#ifndef VIEWER_HH
#define VIEWER_HH

/**
 * @file
 * @brief Definition of Viewer class
 *
 * This file contains definition of Viewer class,
 * which is used for showing visualisation of manipulator.
 */

#include <QGLViewer/qglviewer.h>

/**
 * @brief The Viewer class
 *
 * This class is used for creating visualisation of manipulator
 * in widget in MainWindow. This is derived class form QGLViewer.
 */
class Viewer : public QGLViewer {
 public:
  /**
   * @brief Construcor of Viewer.
   *
   * Construcor of Viewer.
   *
   * @param[in,out] pParent - Pointer to the parent.
   */
  Viewer(QWidget* pParent);

 protected:
  /**
   * @brief Loads texture.
   *
   * Loads texture to given variable, form provided path.
   *
   * @param[in,out] pTexture - Texture variable.
   * @param[in,out] rPath - Path to the texutre file.
   */
  void GLLoadTexture(GLuint* pTexture, const std::string& rPath);

  /**
   * @brief Creates box.
   *
   * Creates box with six walls, empty inside.
   *
   * @param[in] SizeX - Size of the box in X axis.
   * @param[in] SizeY - Size of the box in Y axis.
   * @param[in] SizeZ - Size of the box in Z axis.
   */
  void GLCreateBox(double SizeX, double SizeY, double SizeZ);

  /**
   * @brief Creates cylinder.
   *
   * Creates cylinder of given height and radius.
   *
   * @param Radius - Radius of cylinder.
   * @param Height - Height of cylinder, distance in Z axis.
   */
  void GLCreateCylinder(GLfloat Radius, GLfloat Height);

  /**
   * @brief Initialization of OpenGL.
   *
   * Initialization of OpenGL.
   */
  virtual void init();

  /**
   * @brief Drawing manipulator visualisation in OpenGL.
   *
   * Drawing manipulator visualisation in OpenGL.
   */
  virtual void draw();
};

#endif  // VIEWER_HH
