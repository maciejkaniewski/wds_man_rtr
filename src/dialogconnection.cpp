#include "dialogconnection.hh"

#include <QSerialPortInfo>

#include "ui_dialogconnection.h"

DialogConnection::DialogConnection(QWidget *pParent)
    : QDialog(pParent), _pUi(new Ui::DialogConnection) {
  _pUi->setupUi(this);
}

DialogConnection::~DialogConnection() { delete _pUi; }

void DialogConnection::on_pushButton_search_clicked() {
  emit sendToLogs(tr("Znalezione urządzenia:"));
  QList<QSerialPortInfo> devices;
  devices = QSerialPortInfo::availablePorts();

  _pUi->comboBox_Devices->clear();

  for (int i = 0; i < devices.count(); i++) {
    emit sendToLogs(devices.at(i).portName() + " - " +
                    devices.at(i).description());
    _pUi->comboBox_Devices->addItem(devices.at(i).portName() + " - " +
                                   devices.at(i).description());
  }
}

void DialogConnection::on_pushButton_disconnect_clicked() {
  emit sendDisconnect();
}

void DialogConnection::on_pushButton_connect_clicked() {
  QString portName = _pUi->comboBox_Devices->currentText().split(" - ").first();
  emit sendSerialPortName(portName);
}
